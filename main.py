from functions import m_json
from functions import m_pck



setup_json_path = "/home/pi/calorimetry_home/datasheets/setup_newton.json"
metadata = m_json.get_metadata_from_setup(setup_json_path)


m_pck.check_sensors()
    
    
metadata = {
	"sensor": {
		"values": ["sensor_1", "sensor_2"],
        "serials": ["3ce104579def", "3c88e3811b80"],
        "names": ["Temperature_Sensor_1", "Temperature_Sensor_2"]
    }
}
   
   
    
json_folder = "/home/pi/calorimetry_home/datasheets/setup_newton.json"
data = m_pck.get_meas_data_calorimetry(metadata)
data_folder = "/home/pi/calorimetry_home"
    
m_pck.logging_calorimetry(data, metadata, data_folder, json_folder)
