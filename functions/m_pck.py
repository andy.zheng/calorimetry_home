import os
import sys
import time
import json
from w1thermsensor import Sensor, W1ThermSensor
import h5py
import numpy as np

# This if statement enables you to write and run programs that test functions directly at the end of this file.

if __name__ == "__main__":
    import pathlib

    file_path = os.path.abspath(__file__)
    file_path = pathlib.Path(file_path)
    
    root = file_path.parent.parent
    sys.path.append(str(root))

    from functions import m_json


def check_sensors() -> None:
    """Retrieves and prints the serial number and current temperature of all DS18B20 Temperature Sensors.

    This function utilizes the `w1thermsensor` library to interface with the DS18B20 temperature sensors.
    For each available sensor, it prints its serial number (or ID) and the current temperature reading.

    Examples:
    Assuming two DS18B20 sensors are connected:
    >>> check_sensors()
    Sensor 000005888445 has temperature 25.12
    Sensor 000005888446 has temperature 24.89

    """
    # TODO: Print serials and temperature values of all connected sensors.
    # HINT: Use the W1ThermSensor Library.
    # This line of code throws an exception. This is just to make sure you can see
    # all the code you need to refine. If you already know how to implement the program
    # or have done so, then you can safely delete the three lines of code below, as well
    # as this comment.
    # Iterate over each sensor and print its serial number and temperature
    
    for sensor in W1ThermSensor.get_available_sensors():
        print("Sensor {} has temperature {:.2f}".format(sensor.id, sensor.get_temperature()))


def get_meas_data_calorimetry(metadata: dict) -> dict:
   
    # Initialize an empty dictionary for storing temperature measurements.
    # The structure is uuid: [[temperatures], [timestamps]].
    data = {i: [[], []] for i in metadata["sensor"]["values"]}
    start = time.time()
    sensor_list = [
        W1ThermSensor(Sensor.DS18B20, id) for id in metadata["sensor"]["serials"]
    ]
    input("Press any key to start measurement... <Ctrl+C> to stop measurement")
    try:
        while True:
            for i, sensor in enumerate(sensor_list):
                # TODO: Get experimental data.
                
                # Get experimental data.
                
                # Get current time and temperature
                current_time = time.time() - start
                temperature = sensor.get_temperature()
            
                # Append the current temperature and timestamp to the corresponding lists in the data dictionary
                data[metadata["sensor"]["values"][i]][0].append(temperature)
                data[metadata["sensor"]["values"][i]][1].append(current_time)
            
                # Print the sensor data
                print("Sensor {} has temperature {:.2f} at time {:.2f}s".format(
                    metadata["sensor"]["serials"][i], temperature, current_time))
    
    # Catch the KeyboardInterrupt (e.g., from Ctrl-C) to stop the measurement loop.
    except KeyboardInterrupt:
        # Print the collected data in a formatted JSON structure.
        print(json.dumps(data, indent=4))
    # Always execute the following block.
    finally:
        # Ensure that the lengths of temperature and timestamp lists are the same for each sensor.
        for i in data:
            # If the temperature list is longer, truncate it to match the length of the timestamp list.
            if len(data[i][0]) > len(data[i][1]):
                data[i][0] = data[i][0][0 : len(data[i][1])]
            # If the timestamp list is longer, truncate it to match the length of the temperature list.
            elif len(data[i][0]) < len(data[i][1]):
                data[i][1] = data[i][1][0 : len(data[i][0])]

    return data


def logging_calorimetry(
    data: dict,
    metadata: dict,
    data_folder: str,
    json_folder: str,
) -> None:
    """Logs the calorimetry measurement data into an H5 file.

    This function creates a folder (if not already present) and an H5 file with a
    specific structure. The data from the provided dictionaries are written to the
    H5 file, along with several attributes.

    Args:
        data (dict): Contains sensor data including temperature and timestamp.
                     Refer to README.md section"Runtime measurement data" for a detailed
                     description of the data structure.
        metadata (dict): Contains metadata. Refer to README.md section "Runtime metadata"
                         for a detailed description of the structure.
        data_folder (str): Path to the folder where the H5 file should be created.
        json_folder (str): Path to the folder containing the datasheets.

    """
    # Extract the folder name from the provided path to be used as the H5 file name.
    log_name = data_folder.split("/")[-1]
    # Generate the full path for the H5 file.
    dataset_path = "{}/{}.h5".format(data_folder, log_name)
    # Check and create the logging folder if it doesn't exist.
    if not os.path.exists(data_folder):
        os.makedirs(data_folder)

    # Create a new H5 file.
    f = h5py.File(dataset_path, "w")
    # Create a 'RawData' group inside the H5 file.
    grp_raw = f.create_group("RawData")

    # Add attribute to HDF5.
    # Set attributes for the H5 file based on the datasheets.
    """
    f.attrs["type"] = metadata["type"]
    f.attrs["name"] = metadata["name"]
    for i in ["type", "name"]:
        if f.attrs[i] is None:

    """      
            

    # DONE #

    # TODO: Write data to HDF5.
    

    for uuid, (temperatures, timestamps) in data.items():
    # Create a dataset for each sensor
            grp = grp_raw.create_group(uuid)
            grp.create_dataset("temperature", data=temperatures)
            grp.create_dataset("timestamp", data=timestamps)

    # Close the H5 file.
    f.close()


if __name__ == "__main__":
    # Test and debug.
    check_sensors()
    
    
    metadata = {
        "sensor": {
            "values": ["sensor_1", "sensor_2"],
            "serials": ["3ce104579def", "3c88e3811b80"],
            "names": ["Temperature_Sensor_1", "Temperature_Sensor_2"]
        }
    }
   
   
    
    json_folder = "/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json"
    data = get_meas_data_calorimetry(metadata)
    data_folder = "/home/pi/calorimetry_home"
    
    logging_calorimetry(data, metadata, data_folder, json_folder)
        
    pass
